﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;

namespace StudioKit.Data.Entity.Extensions;

public static class TypeExtensions
{
	private const string DynamicProxyNamespace = "Castle.Proxies";

	/// <summary>
	/// Compare two Entity <see cref="Type"/>s to see if they are equal, and handle if either is an EntityFramework Dynamic Proxy.
	/// </summary>
	/// <param name="x">The first type</param>
	/// <param name="y">The second type</param>
	/// <returns></returns>
	public static bool EntityTypeEquals(this Type x, Type y)
	{
		return GetEntityType(x) == GetEntityType(y);
	}

	/// <summary>
	/// Get the Entity <see cref="Type"/>, accounting for is an EntityFramework Dynamic Proxies.
	/// </summary>
	/// <param name="type">The type of an object</param>
	/// <returns>The non-proxy type</returns>
	public static Type GetEntityType(this Type type)
	{
		return type.Namespace == DynamicProxyNamespace && type.BaseType != null ? type.BaseType : type;
	}
}