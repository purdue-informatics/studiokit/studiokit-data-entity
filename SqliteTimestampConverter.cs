﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Linq;

namespace StudioKit.Data.Entity;

/// <summary>
/// Workaround for EFCore SQLite not handling `[Timestamp]` properly.
/// https://stackoverflow.com/a/52738603
/// </summary>
public class SqliteTimestampConverter : ValueConverter<byte[], string>
{
	public SqliteTimestampConverter() : base(
		v => v == null ? null : ToDb(v),
		v => v == null ? null : FromDb(v))
	{
	}

	private static byte[] FromDb(string v) =>
		v.Select(c => (byte)c).ToArray();

	private static string ToDb(byte[] v) =>
		new(v.Select(b => (char)b).ToArray());

	public static void Configure(ModelBuilder modelBuilder)
	{
		var timestampProperties = modelBuilder.Model
			.GetEntityTypes()
			.SelectMany(t => t.GetProperties())
			.Where(p => p.ClrType == typeof(byte[])
						&& p.ValueGenerated == ValueGenerated.OnAddOrUpdate
						&& p.IsConcurrencyToken);

		foreach (var property in timestampProperties)
		{
			property.SetValueConverter(new SqliteTimestampConverter());
			property.SetDefaultValueSql("CURRENT_TIMESTAMP");
		}
	}
}