using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("StudioKit.Data.Entity.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("StudioKit.Data.Entity.Tests")]
[assembly: AssemblyCopyright("Copyright © 2023 Purdue University")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("47823b7c-b904-41b4-b3a2-eb4296a7e9ee")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]