using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Data.Entity.Tests.TestData;
using StudioKit.Data.Entity.Utils;
using StudioKit.Tests;
using System;

namespace StudioKit.Data.Entity.Tests.Utils;

[TestClass]
public class QueryHintUtilsTests : BaseTest
{
	#region ApplyHints

	[TestMethod]
	public void ApplyHints_ShouldDoNothingWithNoTagComments()
	{
		const string commandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]";
		var result = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(commandText, result);
	}

	[TestMethod]
	public void ApplyHints_ShouldDoNothingWithNoQueryOrTableHintTagComments()
	{
		const string commandText = @"-- some other tag

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(commandText, resultCommandText);
	}

	[TestMethod]
	public void ApplyHints_ShouldApplyQueryHint()
	{
		const string commandText = @"-- query-hint: RECOMPILE

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]";
		const string expectedCommandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
OPTION (RECOMPILE)";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(expectedCommandText, resultCommandText);
	}

	[TestMethod]
	public void ApplyHints_ShouldApplyMultipleQueryHints()
	{
		const string commandText = @"-- query-hint: RECOMPILE
-- query-hint: OPTIMIZE FOR UNKNOWN

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]";
		const string expectedCommandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
OPTION (RECOMPILE, OPTIMIZE FOR UNKNOWN)";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(expectedCommandText, resultCommandText);
	}

	[TestMethod]
	public void ApplyHints_ShouldApplyFromTableHint()
	{
		const string commandText = @"-- table-hint: FROM [TestModelAs], SNAPSHOT

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]";
		const string expectedCommandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t] WITH (SNAPSHOT)";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(expectedCommandText, resultCommandText);
	}

	[TestMethod]
	public void ApplyHints_ShouldApplyInnerJoinIndexTableHint()
	{
		const string commandText = @"-- table-hint: INNER JOIN [TestModelBs], INDEX (IX_TestModelBs_TestModelAId)

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
INNER JOIN [TestModelBs] AS [t0] ON [t].[Id] = [t0].[TestModelAId]";
		const string expectedCommandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
INNER JOIN [TestModelBs] AS [t0] WITH (INDEX (IX_TestModelBs_TestModelAId)) ON [t].[Id] = [t0].[TestModelAId]";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(expectedCommandText, resultCommandText);
	}

	[TestMethod]
	public void ApplyHints_ShouldApplyInnerJoinIndexTableHintWithMultipleIndexes()
	{
		const string commandText = @"-- table-hint: INNER JOIN [TestModelBs], INDEX (IX_TestModelBs_TestModelAId, IX_TestModelBs_Id)

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
INNER JOIN [TestModelBs] AS [t0] ON [t].[Id] = [t0].[TestModelAId]";
		const string expectedCommandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
INNER JOIN [TestModelBs] AS [t0] WITH (INDEX (IX_TestModelBs_TestModelAId, IX_TestModelBs_Id)) ON [t].[Id] = [t0].[TestModelAId]";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(expectedCommandText, resultCommandText);
	}

	[TestMethod]
	public void ApplyHints_ShouldApplyMultipleTableHints()
	{
		const string commandText = @"-- table-hint: FROM [TestModelAs], SNAPSHOT
-- table-hint: INNER JOIN [TestModelBs], INDEX (IX_TestModelBs_TestModelAId)

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
INNER JOIN [TestModelBs] AS [t0] ON [t].[Id] = [t0].[TestModelAId]";
		const string expectedCommandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t] WITH (SNAPSHOT)
INNER JOIN [TestModelBs] AS [t0] WITH (INDEX (IX_TestModelBs_TestModelAId)) ON [t].[Id] = [t0].[TestModelAId]";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(expectedCommandText, resultCommandText);
	}


	[TestMethod]
	public void ApplyHints_ShouldApplyTableHintToAllOccurrences()
	{
		const string commandText = @"-- table-hint: FROM [TestModelAs], SNAPSHOT

SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
WHERE [t].[Value] < 25
UNION ALL
SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t]
WHERE [t].[Value] > 50";
		const string expectedCommandText = @"SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t] WITH (SNAPSHOT)
WHERE [t].[Value] < 25
UNION ALL
SELECT [t].[Id], [t].[Value]
FROM [TestModelAs] AS [t] WITH (SNAPSHOT)
WHERE [t].[Value] > 50";
		var resultCommandText = QueryHintUtils.ApplyHints(commandText);
		Assert.AreEqual(expectedCommandText, resultCommandText);
	}

	#endregion ApplyHints

	#region GetTableNameAndIndexName

	[TestMethod]
	public void GetTableNameAndIndexName_ShouldThrowWhenSourceTypeIsNotInSchema()
	{
		Assert.Throws<InvalidOperationException>(
			() =>
			{
				using var dbContext = DbContextFactory<TestDbContext>.CreateTransient();
				dbContext.GetTableNameAndIndexName<NonDbTestModel, TestModelA>();
			},
			"Entity NonDbTestModel was not found in the DbContext");
	}

	[TestMethod]
	public void GetTableNameAndIndexName_ShouldThrowWhenTargetTypeIsNotInSchema()
	{
		Assert.Throws<InvalidOperationException>(
			() =>
			{
				using var dbContext = DbContextFactory<TestDbContext>.CreateTransient();
				dbContext.GetTableNameAndIndexName<TestModelA, NonDbTestModel>();
			},
			"Entity NonDbTestModel was not found in the DbContext");
	}

	[TestMethod]
	public void GetTableNameAndIndexName_ShouldThrowWhenIndexDoesNotExist()
	{
		Assert.Throws<InvalidOperationException>(
			() =>
			{
				using var dbContext = DbContextFactory<TestDbContext>.CreateTransient();
				dbContext.GetTableNameAndIndexName<TestModelC, TestModelA>();
			},
			"Property TestModelAId was not found for Entity TestModelC");

		Assert.Throws<InvalidOperationException>(
			() =>
			{
				using var dbContext = DbContextFactory<TestDbContext>.CreateTransient();
				dbContext.GetTableNameAndIndexName<TestModelD, TestModelA>();
			},
			"Index IX_TestModelDs_TestModelAId was not found for Entity TestModelD");
	}

	[TestMethod]
	public void GetTableNameAndIndexName_ShouldSucceed()
	{
		using var dbContext = DbContextFactory<TestDbContext>.CreateTransient();
		var (tableName, indexName) = dbContext.GetTableNameAndIndexName<TestModelB, TestModelA>();
		Assert.AreEqual("TestModelBs", tableName);
		Assert.AreEqual("IX_TestModelBs_TestModelAId", indexName);
	}

	#endregion GetTableNameAndIndexName

	#region GetTableInnerJoinIndexHint

	[TestMethod]
	public void GetTableInnerJoinIndexHint_ShouldSucceed()
	{
		using var dbContext = DbContextFactory<TestDbContext>.CreateTransient();
		var hint = dbContext.GetTableInnerJoinIndexHint<TestModelB, TestModelA>();
		Assert.AreEqual("INNER JOIN [TestModelBs], INDEX (IX_TestModelBs_TestModelAId)", hint);
	}

	#endregion GetTableInnerJoinIndexHint
}