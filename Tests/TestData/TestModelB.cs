using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Data.Entity.Tests.TestData;

/// <summary>
/// Has a foreign key relation to <see cref="TestModelA"/>
/// </summary>
public class TestModelB : ModelBase
{
	public int TestModelAId { get; set; }

	[ForeignKey(nameof(TestModelAId))]
	public TestModelA TestModelA { get; set; }

	public int Value { get; set; }
}