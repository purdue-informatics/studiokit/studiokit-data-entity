namespace StudioKit.Data.Entity.Tests.TestData;

/// <summary>
/// Has no relation to <see cref="TestModelA"/>
/// </summary>
public class TestModelC : ModelBase
{
	public int Value { get; set; }
}