namespace StudioKit.Data.Entity.Tests.TestData;

/// <summary>
/// Has no relation to <see cref="TestModelA"/>, but has a property named "TestModelAId"
/// </summary>
public class TestModelD : ModelBase
{
	public int Value { get; set; }

	public int TestModelAId { get; set; }
}