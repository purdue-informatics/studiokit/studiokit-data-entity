using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Utils;
using System.Data.Common;

namespace StudioKit.Data.Entity.Tests.TestData;

public class TestDbContext : DbContext
{
	private readonly DbConnection _dbConnection;
	private readonly string _nameOrConnectionString;

	public TestDbContext()
	{
	}

	public TestDbContext(DbContextOptions options) : base(options)
	{
	}

	public TestDbContext(DbConnection existingConnection)
	{
		_dbConnection = existingConnection;
	}

	public TestDbContext(string nameOrConnectionString)
	{
		_nameOrConnectionString = nameOrConnectionString;
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		if (_dbConnection != null)
		{
			optionsBuilder.UseSqliteOrSqlServer(_dbConnection);
		}
		else if (!string.IsNullOrWhiteSpace(_nameOrConnectionString))
		{
			optionsBuilder.UseSqliteOrSqlServer(_nameOrConnectionString);
		}
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		if (Database.IsSqlite())
		{
			SqliteTimestampConverter.Configure(builder);
		}
	}

	public DbSet<TestModelA> TestModelAs { get; set; }

	public DbSet<TestModelB> TestModelBs { get; set; }

	public DbSet<TestModelC> TestModelCs { get; set; }

	public DbSet<TestModelD> TestModelDs { get; set; }
}