using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Data.Entity.Interfaces;

public interface IBusinessLogicMigration<in TContext>
{
	Task AfterUpAsync(TContext dbContext, ILogger logger, CancellationToken cancellationToken = default);

	Task BeforeDownAsync(TContext dbContext, ILogger logger, CancellationToken cancellationToken = default);
}