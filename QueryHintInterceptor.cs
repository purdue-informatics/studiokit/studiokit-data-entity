using Microsoft.EntityFrameworkCore.Diagnostics;
using StudioKit.Data.Entity.Utils;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Data.Entity;

public class QueryHintInterceptor : DbCommandInterceptor
{
	public override InterceptionResult<DbDataReader> ReaderExecuting(DbCommand command, CommandEventData eventData,
		InterceptionResult<DbDataReader> result)
	{
		command.ApplyHints();
		return base.ReaderExecuting(command, eventData, result);
	}

	public override ValueTask<InterceptionResult<DbDataReader>> ReaderExecutingAsync(DbCommand command, CommandEventData eventData,
		InterceptionResult<DbDataReader> result, CancellationToken cancellationToken = default)
	{
		command.ApplyHints();
		return base.ReaderExecutingAsync(command, eventData, result, cancellationToken);
	}

	public override InterceptionResult<object> ScalarExecuting(DbCommand command, CommandEventData eventData,
		InterceptionResult<object> result)
	{
		command.ApplyHints();
		return base.ScalarExecuting(command, eventData, result);
	}

	public override ValueTask<InterceptionResult<object>> ScalarExecutingAsync(DbCommand command, CommandEventData eventData,
		InterceptionResult<object> result, CancellationToken cancellationToken = default)
	{
		command.ApplyHints();
		return base.ScalarExecutingAsync(command, eventData, result, cancellationToken);
	}
}