using Microsoft.Data.SqlClient;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace StudioKit.Data.Entity.Utils;

public static class DbContextUtils
{
	public const string SqliteConnectionStringPrefix = "Filename=";

	/// <summary>
	/// Use Sqlite if the connection string is a Sqlite connection string
	/// </summary>
	/// <param name="connectionString"></param>
	/// <returns></returns>
	public static bool ShouldUseSqlite(string connectionString)
	{
		// use Sqlite if the connectionString prefix matches
		return !string.IsNullOrWhiteSpace(connectionString) && connectionString.StartsWith(SqliteConnectionStringPrefix);
	}

	public static DbContextOptionsBuilder UseSqliteOrSqlServer(this DbContextOptionsBuilder optionsBuilder, DbConnection dbConnection)
	{
		switch (dbConnection)
		{
			case SqliteConnection:
				optionsBuilder.UseSqlite(dbConnection, o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery));
				break;

			case SqlConnection:
				optionsBuilder.UseSqlServer(dbConnection, o =>
				{
					o.EnableRetryOnFailure();
					o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery);
				});
				break;
		}

		return optionsBuilder;
	}

	/// <summary>
	/// Automatically chooses the database provider, Sqlite or SqlServer, based on the <paramref name="connectionString"/>.
	/// </summary>
	/// <param name="optionsBuilder"></param>
	/// <param name="connectionString"></param>
	public static DbContextOptionsBuilder UseSqliteOrSqlServer(this DbContextOptionsBuilder optionsBuilder, string connectionString)
	{
		if (ShouldUseSqlite(connectionString))
		{
			optionsBuilder.UseSqlite(connectionString, o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery));
		}
		else
		{
			optionsBuilder.UseSqlServer(connectionString, o =>
			{
				o.EnableRetryOnFailure();
				o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery);
			});
			optionsBuilder.AddQueryHints();
		}

		return optionsBuilder;
	}
}