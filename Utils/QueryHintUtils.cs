using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace StudioKit.Data.Entity.Utils;

public static class QueryHintUtils
{
	private const string TableHintTag = "table-hint: ";
	private const string QueryHintTag = "query-hint: ";

	private static Regex TableAliasRegex(string tableName) => new(
		@$"({Regex.Escape(tableName)}(?:[\s\r\n]+AS[\s\r\n]+[^\s\r\n]+)?)",
		RegexOptions.Multiline | RegexOptions.IgnoreCase);

	/// <summary>
	/// <para>
	/// Apply all table hints and query hints that exist as query tags, e.g. `-- table-hint: ...`, `-- query-hint: ...`. All table hint
	/// and query hint tags will be removed from the final command text.
	/// </para>
	/// <para>See <see cref="WithTableHint{T}"/> and <see cref="WithHint{T}"/>.</para>
	/// </summary>
	/// <param name="commandText">The commandText to modify.</param>
	public static string ApplyHints(string commandText)
	{
		if (!commandText.StartsWith("--")) return commandText;

		var tableHints = Regex.Matches(commandText, @"-- table-hint: ([a-zA-Z\[\]\s\r\n]+), (.+)[\r\n]+");
		var queryHints = Regex.Matches(commandText, @"-- query-hint: (.+)[\r\n]+");

		if (!tableHints.Any() && !queryHints.Any()) return commandText;

		// remove hint comments from command text
		var endOfHintsIndex = tableHints
			.Concat(queryHints)
			.Select(m => m.Index + m.Length)
			.Max();
		var updatedCommandText = commandText[endOfHintsIndex..].TrimStart();

		foreach (Match tableHint in tableHints)
		{
			// find usages of the table name expression in the query and append `WITH (...)` after each occurence
			// regex accounts for table alias
			var tableName = tableHint.Groups[1].Value;
			var hint = tableHint.Groups[2].Value.Trim();
			var commandTextWithTableHint = TableAliasRegex(tableName)
				.Replace(updatedCommandText, $"${{0}} WITH ({hint})");
			updatedCommandText = commandTextWithTableHint;
		}

		if (queryHints.Any())
		{
			// join all query hints together into a single `OPTION(...)` command and append it to the query
			var queryHintsText = string.Join(", ", queryHints.Select(m => m.Groups[1].Value.Trim()));
			var commandTextWithQueryHint = @$"{updatedCommandText}
OPTION ({queryHintsText})";
			updatedCommandText = commandTextWithQueryHint;
		}

		return updatedCommandText;
	}

	#region IDbCommand Extensions

	/// <summary>
	/// <para>
	/// Apply all table hints and query hints that exist as query tags, e.g. `-- table-hint: ...`, `-- query-hint: ...`. All table hint
	/// and query hint tags will be removed from the final command text.
	/// </para>
	/// <para>See <see cref="WithTableHint{T}"/> and <see cref="WithHint{T}"/>.</para>
	/// </summary>
	/// <param name="command">The command to modify.</param>
	public static void ApplyHints(this IDbCommand command)
	{
		command.CommandText = ApplyHints(command.CommandText);
	}

	#endregion IDbCommand Extensions

	#region DbContext Extensions

	/// <summary>
	/// Get the schema table name for <typeparamref name="TSource"/> and the index name for the foreign key index to <typeparamref name="TTarget"/>  (by naming convention).
	/// </summary>
	/// <exception cref="InvalidOperationException">Throws if<typeparamref name="TSource"/> or <typeparamref name="TTarget"/> are not registered in the schema.</exception>
	public static (string tableName, string indexName) GetTableNameAndIndexName<TSource, TTarget>(this DbContext context)
	{
		var entityType = context.Model.FindEntityType(typeof(TSource));
		var tableName = entityType?.GetSchemaQualifiedTableName();
		if (string.IsNullOrWhiteSpace(tableName))
			throw new InvalidOperationException($"Entity {typeof(TSource).ShortDisplayName()} was not found in the DbContext");

		var targetEntityType = context.Model.FindEntityType(typeof(TTarget));
		if (targetEntityType == null)
			throw new InvalidOperationException($"Entity {typeof(TTarget).ShortDisplayName()} was not found in the DbContext");

		// assumes standard naming convention for FK property name and index
		var foreignKeyPropertyName = $"{targetEntityType.ShortName()}Id";
		var indexName = $"IX_{tableName}_{foreignKeyPropertyName}";
		var property = entityType.FindProperty(foreignKeyPropertyName);
		if (property == null)
			throw new InvalidOperationException(
				$"Property {foreignKeyPropertyName} was not found for Entity {typeof(TSource).ShortDisplayName()}");
		if (entityType.FindIndex(property)?.GetMappedTableIndexes().FirstOrDefault(i => i.Name.Equals(indexName)) == null)
			throw new InvalidOperationException($"Index {indexName} was not found for Entity {typeof(TSource).ShortDisplayName()}");

		return (tableName, indexName);
	}

	/// <summary>
	/// Get a hint in the form of `INNER JOIN [{tableName}], INDEX ({indexName})`, where tableName is the schema table name for <typeparamref name="TSource"/>,
	/// and indexName is the foreign key index name to <typeparamref name="TTarget"/> (by naming convention).
	/// </summary>
	public static string GetTableInnerJoinIndexHint<TSource, TTarget>(this DbContext context)
	{
		var (tableName, indexName) = context.GetTableNameAndIndexName<TSource, TTarget>();
		return $"INNER JOIN [{tableName}], INDEX ({indexName})";
	}

	#endregion DbContext Extensions

	#region IQueryable Extensions

	/// <summary>
	/// <para>Tag the query with a query hint comment, which will be applied to the query by the <see cref="QueryHintInterceptor"/>.</para>
	/// <para>The hint comment will take the form of `-- query-hint: {hint}`. The hint will be wrapped in an `OPTION` command and appended to the query, e.g. `OPTION ({hint})`.</para>
	/// <para>If a query is tagged with multiple query hints, they will all be appended inside one `OPTION` command, e.g. `OPTION ({hint1}, {hint2})`</para>
	/// </summary>
	/// <param name="source">The source query.</param>
	/// <param name="hint">A query hint that will be wrapped in an `OPTION` command and appended to the query.</param>
	public static IQueryable<T> WithHint<T>(this IQueryable<T> source, string hint) =>
		source.TagWith($"{QueryHintTag}{hint}");

	/// <summary>
	/// <para>Tag the query with a table hint comment, which will be applied to the query by the <see cref="QueryHintInterceptor"/>.</para>
	/// <para>
	/// The hint comment will take the form of `-- table-hint: {hint}`, and the `hint` should be in the form `{tableNameExpression}, {tableHint}`.
	/// The hint will be wrapped in a `WITH` command and inserted after each occurence of {tableNameExpression}, and any table alias.
	/// </para>
	/// <para>
	/// e.g. given the hint `FROM [Table1], INDEX (IX_Table1_Table2Id)`, all instances of `FROM [Table1] AS [t]` (or other alias)
	/// will be replaced with `FROM [Table1] AS [t] WITH (INDEX (IX_Table1_Table2Id))`
	/// </para>
	/// <para>Multiple table hints for the same table should be provided in a single tag.</para>
	/// </summary>
	/// <param name="source">The source query.</param>
	/// <param name="hint">A table hint in the form of `{tableNameExpression}, {tableHint}`.</param>
	public static IQueryable<T> WithTableHint<T>(this IQueryable<T> source, string hint) =>
		source.TagWith($"{TableHintTag}{hint}");

	#endregion IQueryable Extensions

	#region DbContextOptionsBuilder Extensions

	public static DbContextOptionsBuilder AddQueryHints(this DbContextOptionsBuilder builder) =>
		builder.AddInterceptors(new QueryHintInterceptor());

	#endregion DbContextOptionsBuilder Extensions
}