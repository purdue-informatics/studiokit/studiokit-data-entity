using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;

namespace StudioKit.Data.Entity;

public class DbUpdateAndSqlServerRetryingExecutionStrategy : SqlServerRetryingExecutionStrategy
{
	public DbUpdateAndSqlServerRetryingExecutionStrategy(DbContext context) : base(context)
	{
	}

	public DbUpdateAndSqlServerRetryingExecutionStrategy(ExecutionStrategyDependencies dependencies) : base(dependencies)
	{
	}

	public DbUpdateAndSqlServerRetryingExecutionStrategy(DbContext context, int maxRetryCount) : base(context, maxRetryCount)
	{
	}

	public DbUpdateAndSqlServerRetryingExecutionStrategy(ExecutionStrategyDependencies dependencies, int maxRetryCount) : base(dependencies,
		maxRetryCount)
	{
	}

	public DbUpdateAndSqlServerRetryingExecutionStrategy(ExecutionStrategyDependencies dependencies, IEnumerable<int> errorNumbersToAdd) :
		base(dependencies, errorNumbersToAdd)
	{
	}

	public DbUpdateAndSqlServerRetryingExecutionStrategy(DbContext context, int maxRetryCount, TimeSpan maxRetryDelay,
		IEnumerable<int> errorNumbersToAdd) : base(context, maxRetryCount, maxRetryDelay, errorNumbersToAdd)
	{
	}

	public DbUpdateAndSqlServerRetryingExecutionStrategy(ExecutionStrategyDependencies dependencies, int maxRetryCount,
		TimeSpan maxRetryDelay, IEnumerable<int> errorNumbersToAdd) : base(dependencies, maxRetryCount, maxRetryDelay, errorNumbersToAdd)
	{
	}

	protected override bool ShouldRetryOn(Exception exception)
	{
		return exception is DbUpdateException || base.ShouldRetryOn(exception);
	}
}